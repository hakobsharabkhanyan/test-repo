<?php 
namespace UserGroup;

use \User;
use \Group;
use \Sentry;
use \Exception;
use \Cartalyst\Sentry\Users\UserNotFoundException;

class Helpers{

	/*
	 * Check if user has only read permissions or not.
	 *
	 * @return bool
	 */
	public static function isUserReadOnly()
	{
		if( Sentry::check() ){
			$user = Sentry::getUser();
			if( $user->hasAccess('admin') || $user->hasAccess('super') ){
				return false;
			}
			else{
				return true;
			}
		}
		
		
	}
}