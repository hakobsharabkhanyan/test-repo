<?php namespace UserGroup\Contracts;

interface UserInterface{

	/*
	 * @param integer $id
	 * @param integer $groupId
	 * 
	 * @return bool
	 */
	public function changePermissionsTo( $id, $groupId );
}