<?php namespace UserGroup\Services;

use UserGroup\Contracts\UserInterface;
use \User;
use \Sentry;
class UserService implements UserInterface{

	/*
	 * @param integer $id
	 * @param integer $groupId
	 * 
	 * @return bool
	 */
	public function changePermissionsTo( $id, $groupId )
	{
		$user = Sentry::findUserById($id);

	    $group = Sentry::findGroupById($groupId); 
		return $user->addGroup($group);
	}
}