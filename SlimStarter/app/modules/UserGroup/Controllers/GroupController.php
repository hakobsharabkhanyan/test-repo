<?php

namespace UserGroup\Controllers;

use \App;
use \View;
use \Menu;
use \User;
use \Group;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Admin\BaseController;
use \Cartalyst\Sentry\Users\UserNotFoundException;

class GroupController extends BaseController
{
    private $isReadOnly;

    public function __construct()
    {
        parent::__construct();
        Menu::get('admin_sidebar')->setActiveMenu('group');
    }

    public function index()
    {
        $this->data['title'] = 'Group List';

        $user = Sentry::getUser();
        $this->data['canCreate'] = $user->hasAccess('group.create');
        $this->data['canUpdate'] = $user->hasAccess('group.edit');
        $this->data['canDelete'] = $user->hasAccess('group.delete');
        $this->data['canView'] = $user->hasAccess('group.view');
        /** render the template */
        $user = Sentry::getUser();
        $groups = Sentry::findAllGroups();
        $returnGroup = [];
        foreach ($groups as $key => $group) {
            $perms = array_keys( (array)$group->getPermissions()  );
            $returnGroup[] = ['name' => $group->name, 'id' => $group->id, 'permissions' => implode(',', $perms) ];
        }
        $this->data['groups'] = $returnGroup;

        /** load the user.js app */
        $this->loadJs('app/group.js');

        /** publish necessary js  variable */
        $this->publish('baseUrl', $this->data['baseUrl']);

        /** render the template */

        View::display('@usergroup/group/index.twig', $this->data);
    }

    public function show($id)
    {
        if(Request::isAjax()){
            $group = null;
            $message = '';
            $returnArray = null;
            try{
                $currentUser = Sentry::getUser();
                if( !$currentUser->hasAccess('group.view') )
                {
                    throw new Exception('You don\'t have permissions for this action', 1);
                }

                $group = Sentry::findGroupById($id);
                $perms = array_keys( $group->getPermissions()  );
                $permissions = implode("\n", $perms);
                
                $returnArray = [
                    'id' => $group->id,
                    'name' => $group->name,
                    'permissions' => $permissions
                ];
            }catch(Exception $e){
                $message = $e->getMessage();
            }
            
            Response::headers()->set('Content-Type', 'application/json');
            Response::setBody(json_encode(
                array(
                    'success'   => !is_null($group),
                    'data'      => $returnArray,
                    'message'   => $message,
                    'code'      => is_null($group) ? 404 : 200
                )
            ));
        }else{

        }
    }

    public function store()
    {
        
        $group    = null;
        $message = '';
        $success = false;
        $returnArray = null;
        $update = false;
        $delete = false;
        try{
            $currentUser = Sentry::getUser();
            if( !$currentUser->hasAccess('group.create') )
            {
                throw new Exception('You don\'t have permissions for this action', 1);
            }
            if( $currentUser->hasAccess('group.edit') ){
                $update = true;
            }
            if( $currentUser->hasAccess('group.delete') ){
                $delete = true;
            }
            $input = Input::post();
            
            $input['permissions'] = str_replace("\r",",",$input['permissions']);
            $input['permissions'] = str_replace("\n",",",$input['permissions']);
            $input['permissions'] = str_replace("\t",",",$input['permissions']);
            $input['permissions'] = preg_replace('/\s+/', ',', $input['permissions']);
            $permissions = explode( ',', $input['permissions']  );

            array_walk($permissions, function(&$value) {
              $value = trim($value);
            });
            $permissions = array_filter($permissions);
            $permissions = array_unique($permissions);

            $array = [];
            foreach ($permissions as $perm) {
                if( $perm == "" ){
                    continue;
                }
                $array[$perm] = 1 ;
            }
            $name = $input['name'];
            $group = Sentry::createGroup(array(
                'name'        => $name,
                'permissions' => $array
            ));
            //$group->users()->attach( Sentry::getUser()->id );
            array_walk($permissions, function(&$value) {
              $value = htmlentities($value);
            });
            $returnArray = [
                'id' => $group->id,
                'name' => htmlentities( $group->name ),
                'permissions' => implode(',', $permissions)
            ];
            $success = true;
            $message = 'Group created successfully';
        }catch (Exception $e){
            $message = $e->getMessage();
        }

        if(Request::isAjax()){
            Response::headers()->set('Content-Type', 'application/json');
            Response::setBody(json_encode(
                array(
                    'success'   => $success,
                    'data'      => $returnArray,
                    'permissions'   => [ 'update' => $update, 'delete' => $delete ],
                    'message'   => $message,
                    'code'      => $success ? 200 : 500
                )
            ));
        }else{
            Response::redirect($this->siteUrl('admin/group'));
        }
    }

    public function create()
    {

    }

    public function edit()
    {

    }

    public function update($id)
    {
        
        $id = (int) $id;
        $success = false;
        $message = '';
        $group    = null;
        $code    = 200;
        $returnArray = null;
        try{
            $currentUser = Sentry::getUser();
            if( !$currentUser->hasAccess('group.edit') )
            {
                throw new Exception('You don\'t have permissions for this action', 1);
            }
            $input = Input::put();
            /** in case request come from post http form */
            $input = is_null($input) ? Input::post() : $input;

            /*array_walk($input, function(&$value) {
              $value = strip_tags($value);
            });*/

            $group = Sentry::findGroupById($id);
            $group->name = $input['name'];
            
            $input['permissions'] = str_replace("\r",",",$input['permissions']);
            $input['permissions'] = str_replace("\n",",",$input['permissions']);
            $input['permissions'] = str_replace("\t",",",$input['permissions']);
            $input['permissions'] = preg_replace('/\s+/', ',', $input['permissions']);
            $permissions = explode( ',', $input['permissions']  );

            array_walk($permissions, function(&$value) {
              $value = trim($value);
            });
            $permissions = array_filter($permissions);
            $permissions = array_unique($permissions);
            $array = [];

            foreach ($group->getPermissions() as $key => $value) {
                $array[$key] = 0;
            }
            foreach ($permissions as $perm) {
                $array[$perm] = 1 ;
            }
            $group->permissions = $array;
            $success = $group->save();
            $code    = 200;
            $message = 'Group updated sucessully';

            array_walk($permissions, function(&$value) {
              $value = htmlentities($value);
            });
            $returnArray = [
                'id' => $id,
                'name' => htmlentities( $group->name ),
                'permissions' => implode( ',', $permissions )
            ];

        }catch (Exception $e){
            $message = $e->getMessage();
            //$code    = 500;
        }
        

        if(Request::isAjax()){
            Response::headers()->set('Content-Type', 'application/json');
            Response::setBody(json_encode(
                array(
                    'success'   => $success,
                    'data'      => $returnArray,
                    'message'   => $message,
                    'code'      => $code
                )
            ));
        }else{
            Response::redirect($this->siteUrl('admin/group/'.$id.'/edit'));
        }
    }

    public function destroy($id)
    {
        
        $deleted = false;
        $message = '';
        $code    = 0;

        try{
            $currentUser = Sentry::getUser();
            if( !$currentUser->hasAccess('group.delete') )
            {
                throw new Exception('You don\'t have permissions for this action', 1);
            }
            $group    = Group::find($id);
            //$group->users()->detach( Sentry::getUser()->id );
            $deleted = $group->delete();
            $code    = 200;
        }catch(Exception $e){
            $message = $e->getMessage();
            $code    = 500;
        }

        if(Request::isAjax()){
            Response::headers()->set('Content-Type', 'application/json');
            Response::setBody(json_encode(
                array(
                    'success'   => $deleted,
                    'data'      => array( 'id' => $id ),
                    'message'   => $message,
                    'code'      => $code
                )
            ));
        }else{
            Response::redirect($this->siteUrl('admin/group'));
        }
    }
}