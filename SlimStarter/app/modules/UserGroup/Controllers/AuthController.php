<?php 
namespace UserGroup\Controllers;

use \App;
use \View;
use \Menu;
use \User;
use \Group;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Admin\BaseController;
use \Cartalyst\Sentry\Users\UserNotFoundException;
//use \swiftmailer\swiftmailer\lib\classes\Swift\MailTransport as Swift_SmtpTransport;

require_once __DIR__ . '/../../../../vendor/swiftmailer/swiftmailer/lib/swift_required.php';

class AuthController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        
    }

    public function show($id)
    {

        
    }

    /**
	 * Show the form for creating a new user.
	 *
	 */
    public function create()
    {
    	$this->data['title'] = 'Registration';

        $this->data['submit_url'] = $this->siteUrl('doregister');

        $this->publish('baseUrl', $this->data['baseUrl']);
        View::display('@usergroup/auth/index.twig', $this->data);

    }

    public function checkCode($code)
    {
        $user = Sentry::findUserByActivationCode($code);
        if( !$user ){
            Response::redirect($this->siteUrl('register'));
        }
        if ($user->attemptActivation($code))
        {
            $message = 'succeed';
        }
        else
        {
            $message = "Failed";
        }
        Response::redirect($this->siteUrl('login'));
    }

    /**
     * Show the form for password reminder.
     *
     */
    public function getPasswordReminder()
    {
        $this->data['title'] = 'Password reset';

        $this->data['submit_url'] = $this->siteUrl('password-reminder');

        $this->publish('baseUrl', $this->data['baseUrl']);
        View::display('@usergroup/auth/reminder.twig', $this->data);
    }

    /**
     * Show the form for creating new password.
     *
     */
    public function getPasswordReminderFinal($code)
    {
        $this->data['title'] = 'Password reset';
        $this->data['submit_url'] = $this->siteUrl('password-reminder-final');

        $this->publish('baseUrl', $this->data['baseUrl']);
        $this->data['code'] = $code;
        View::display('@usergroup/auth/set_new_password.twig', $this->data);
    }

     /**
     * Change password.
     *
     */
    public function postPasswordReminderFinal()
    {
        $user    = null;
        $message = '';
        $success = false;

        try{
            $input = Input::post();
            $code = $input['code'];
            if($input['password'] != $input['confirm_password']){
                throw new Exception("Password and confirmation password not match", 1);
            }

            $user = Sentry::findUserByResetPasswordCode($code);
            if( !$user ){
                throw new Exception("User not found", 1);
            }

            if ($user->checkResetPasswordCode($code))
            {
                if ($user->attemptResetPassword($code, $input['password']))
                {
                    $message = 'succeed';
                }
                else
                {
                    throw new Exception("Failed", 1);
                }
            }
            else
            {
                throw new Exception("Invalid token", 1);
            }
            $user->save();
            $success = true;

            App::flash('success', $message);
        }catch (Exception $e){
            $message = $e->getMessage();
            App::flash('error', $message);
        }

        
        Response::redirect($this->siteUrl('register'));
    }

    /**
     * Process the data for password reminder.
     *
     */
    public function postPasswordReminder()
    {
        try{
            $input = Input::post();


            $email = trim( $input['email'] );


            $user = Sentry::findUserByLogin($email);
            if( !$user ){
                throw new Exception("User not found", 1);
            }

            $code = $this->generateRandomString(20);
            $user->reset_password_code = $code;
            $user->save();

            

            $to = [ $input['email'] ];
            $body = 'Follow this link to change your password' . $this->siteUrl('get-password-reminder-code') . '/' . $code;
            $subject = 'Password Reminder';
            $result = $this->sendMail( $to, $body, $subject );


            $message = 'Reminder Sent';
            App::flash('success', $message);
        }catch (Exception $e){
            $message = $e->getMessage();
            App::flash('error', $message);
        }
        Response::redirect($this->siteUrl('get-password-reminder'));
    }

    /**
	 * Store a newly created user into database.
	 *
	 */
    public function store()
    {
        $user    = null;
        $message = '';
        $success = false;

        try{
            $input = Input::post();

            if($input['password'] != $input['confirm_password']){
                throw new Exception("Password and confirmation password not match", 1);
            }


            array_walk($input, function(&$value) {
                $value = trim($value);
                $value = strip_tags($value);
            });

            $user = Sentry::createUser(array(
                'email'       => $input['email'],
                'password'    => $input['password'],
                'first_name'  => $input['first_name'],
                'last_name'   => $input['last_name'],
                'activated'   => 0
            ));
            $code = $this->generateRandomString(20);
            $user->activation_code = $code;
            $user->save();
            $success = true;


            $to = [ $input['email'] => $input['first_name'] . $input['last_name'] ];
            $body = 'You have successfully registered . Go to this link to activate your account' . $this->siteUrl('activate') . '/' . $code;
            $subject = 'Activation';
            $result = $this->sendMail( $to, $body, $subject );


            $message = 'You have been successfully registered . Check your email to activate your account.';
            App::flash('success', $message);
        }catch (Exception $e){
            $message = $e->getMessage();
            App::flash('error', $message);
        }

        
        Response::redirect($this->siteUrl('register'));
    }


    private function sendMail( $to, $body, $subject  )
    {
        $transport = \Swift_SmtpTransport::newInstance('smtp.mandrillapp.com', 587)
              ->setUsername('hakobsharabkhanyan@gmail.com')
              ->setPassword('I4VpnZsbFCs41HltbFOTzA');

        $mailer = \Swift_Mailer::newInstance($transport);
        $message = \Swift_Message::newInstance($subject)
          ->setFrom(array('hakobsharabkhanyan@gmail.com' => 'Hakob'))
          ->setTo( $to )
          ->setBody( $body );

        $result = $mailer->send($message);
        return $result;
    }

    private function generateRandomString($length = 20) 
    {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}


    public function edit()
    {

    }

    public function update($id)
    {
        
    }

    public function destroy($id)
    {
        
    }

}