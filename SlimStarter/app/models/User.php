<?php

class User extends Model {

	public function groups(){
		return $this->belongsToMany('Group', 'users_groups');
	}
}