<?php

/**
 * Sample group routing with user check in middleware
 */
Route::group(
    '/admin',
    function(){
        if(!Sentry::check()){

            if(Request::isAjax()){
                Response::headers()->set('Content-Type', 'application/json');
                Response::setBody(json_encode(
                    array(
                        'success'   => false,
                        'message'   => 'Session expired or unauthorized access.',
                        'code'      => 401
                    )
                ));
                App::stop();
            }else{
                $redirect = Request::getResourceUri();
                Response::redirect(App::urlFor('login').'?redirect='.base64_encode($redirect));
            }
        }
    },
    function() use ($app) {
        /** sample namespaced controller */
        Route::get('/', 'Admin\AdminController:index')->name('admin');

        foreach (Module::getModules() as $module) {
            $module->registerAdminRoute();
        }
    }
);

Route::get('/login', 'Admin\AdminController:login')->name('login');
Route::get('/logout', 'Admin\AdminController:logout')->name('logout');
Route::post('/login', 'Admin\AdminController:doLogin');

/** Route to documentation */
Route::get('/doc(/:page+)', 'DocController:index');

Route::get('/register', 'UserGroup\Controllers\AuthController:create');
Route::post('/doregister', 'UserGroup\Controllers\AuthController:store');
Route::get('/activate/:code', 'UserGroup\Controllers\AuthController:checkCode');
Route::get('/get-password-reminder', 'UserGroup\Controllers\AuthController:getPasswordReminder');
Route::get('/get-password-reminder-code/:code', 'UserGroup\Controllers\AuthController:getPasswordReminderFinal');
Route::post('/password-reminder', 'UserGroup\Controllers\AuthController:postPasswordReminder');
Route::post('/password-reminder-final', 'UserGroup\Controllers\AuthController:postPasswordReminderFinal');

foreach (Module::getModules() as $module) {
    $module->registerPublicRoute();
}

/** default routing */
Route::get('/', 'HomeController:welcome');